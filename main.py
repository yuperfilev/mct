import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
import pandas


def show_B(x, y0, y1=[]):
    fig = plt.figure(figsize = (7, 4))
    ax = fig.add_subplot(111)
    line1 = ax.plot(x, y0, 'r')
    ax.grid(axis = 'both')
    if len(y1) != 0:
        line2 = ax.plot(x, y1, 'g--')
    ax.set_xlabel('x')
    ax.set_ylabel('Bz')
    plt.show()


def data():
    with open("ox.txt") as f:
        a, b, num_listener = f.readline().split()
    listeners = np.linspace(int(a), int(b), int(num_listener))
    with open("cells.txt") as f:
        cells = np.array(f.readline().split(), dtype="int")
        point_lu = np.array(list(map(float, f.readline().split())))
        point_rd = np.array(list(map(float, f.readline().split())))
    with open("p.txt") as magnetization_file:
        p_magnetization = np.zeros(3 * cells[0] * cells[1])
        lines = 0
        bounds = list(map(float, magnetization_file.readline().split()))
        for magnetic in magnetization_file:
            data_line = magnetic.split()
            p_cell_value = data_line[:3]

            if len(data_line) == 4:
                # x_val y_val z_val repeat_cell
                repeated_magnetic_len = int(data_line[3])
                for i in range(repeated_magnetic_len):
                    p_magnetization[3 * (lines + i):3 * (lines + i + 1)
                                    ] = np.array(list(map(float, p_cell_value)))
                lines += repeated_magnetic_len
            else:
                # x_val y_val z_val
                p_magnetization[3 * lines:3 *
                                (lines + 1)] = np.array(list(map(float, p_cell_value)))
                lines += 1
    return cells, listeners, point_lu, point_rd, p_magnetization, bounds


def dist(point1, point2):
    return np.sqrt((point2[0]-point1[0])**2 + (point2[1]-point1[1])**2 + (point2[2]-point1[2])**2)


def calc_center(cells, point_lu, point_rd):
    width = point_rd[0] - point_lu[0]
    height = point_rd[2] - point_lu[2]
    cellsCenter = np.zeros((cells[0]*cells[1], 3))
    k = 0
    x = point_lu[0]
    z = point_lu[2]
    for i in range(cells[1]):
        for j in range(cells[0]):
            cellsCenter[k] = [x + width/2, 0, z + height/2]
            k += 1
            x += width
        x = point_lu[0]
        z += height
    return cellsCenter

# прямая задача


def forward_problem(listeners, mes, cells_center, p):
    n = len(listeners)
    num_cells = len(cells_center)
    L = np.zeros((n, 3*num_cells, 3))
    B = np.zeros((n, 3))
    i = 0
    y = 0
    z = 0
    for x in listeners:
        for k in range(num_cells):
            r = dist([x, 0, z], cells_center[k])
            _x = x - cells_center[k, 0]
            _y = y - cells_center[k, 1]
            _z = z - cells_center[k, 2]

            L[i, 3*k, 0] = (3*_x**2)/r**2 - 1
            L[i, 3*k, 1] = (3*_x * _y)/r**2
            L[i, 3*k, 2] = (3*_x * _z)/r**2

            L[i, 1+3*k, 0] = (3*_x * _y)/r**2
            L[i, 1+3*k, 1] = (3*_y**2)/r**2 - 1
            L[i, 1+3*k, 2] = (3*_y * _z)/r**2

            L[i, 2+3*k, 0] = (3*_x * _z)/r**2
            L[i, 2+3*k, 1] = (3*_y * _z)/r**2
            L[i, 2+3*k, 2] = (3*_z**2)/r**2 - 1

            L[i, 3*k, :] *= mes/(4*np.pi*r**3)
            L[i, 1+3*k, :] *= mes/(4*np.pi*r**3)
            L[i, 2+3*k, :] *= mes/(4*np.pi*r**3)

            B[i, 0] += (L[i, 3*k, 0]*p[3*k] + L[i, 1+3*k, 0]
                        * p[1+3*k] + L[i, 2+3*k, 0]*p[2+3*k])
            B[i, 1] += (L[i, 3*k, 1]*p[3*k] + L[i, 1+3*k, 1]
                        * p[1+3*k] + L[i, 2+3*k, 1]*p[2+3*k])
            B[i, 2] += (L[i, 3*k, 2]*p[3*k] + L[i, 1+3*k, 2]
                        * p[1+3*k] + L[i, 2+3*k, 2]*p[2+3*k])
        i += 1
    return L, B

# обратная задача


def inverse_problem(L, B, num_cells, n):
    A = np.zeros((3*num_cells, 3*num_cells))
    b = np.zeros(3*num_cells,)
    for q in range(3*num_cells):
        for s in range(3*num_cells):
            A[q, s] = sum(np.dot(L[i, q], L[i, s]) for i in range(n))
        b[q] = sum(np.dot(L[i, q], B[i]) for i in range(n))
    return A, b

# Вычисление функционала
def regularized_functional(B, Bp, alpha, p):
    n = B.shape[0]
    k = int(len(p)/3)
    functional = sum((B[i, 0]-Bp[i, 0])**2 + (B[i, 1]-Bp[i, 1])**2 + (B[i, 2]-Bp[i, 2])**2 for i in range(n)) + \
        alpha*sum(p[3*i]**2 + p[3*i+1]**2 + p[3*i+2]**2 for i in range(k))
    return functional


def main():
    functionals_ratio = 0.03
    alpha = 1e-15

    cells, listeners, point_lu, point_rd, p, bounds = data()
    num_cells = cells[0]*cells[1]
    n = len(listeners)
    cells_center = calc_center(cells, point_lu, point_rd)
    mes = abs(point_rd[0] - point_lu[0])*abs(point_rd[2] - point_lu[2])

    L, B = forward_problem(listeners, mes, cells_center, p)
    A, b = inverse_problem(L, B, num_cells, n)
    p_computed = np.linalg.solve(A, b)
    _, Bp = forward_problem(listeners, mes, cells_center, p_computed)
    F0 = regularized_functional(B, Bp, alpha, p_computed)
    print(f"Functional with alpha 0: {F0}")
    px = p_computed[2::3].reshape((cells[1], cells[0]))
    np.savetxt('result.txt', px, comments='# ', header=f'a = 0, Fp = {F0}')
    k = 0
    I = np.eye(3*num_cells)
    while True:
        p_next = np.linalg.solve(A + alpha * I, b)
        _, Bp = forward_problem(listeners, mes, cells_center, p_next)
        Fp = regularized_functional(B, Bp, alpha, p_next)
        print(f"Functional with alpha {alpha}: {Fp}")
        print(Fp - F0)
        px = p_next[2::3].reshape((cells[1], cells[0]))
        np.savetxt(f'result{k}.txt', px, comments="# ",
                   header=f'a = {alpha}, Fp = {Fp}')
        if Fp/F0 > functionals_ratio or all([bounds[0] <= x <= bounds[1] for x in p_next]):
            break
        p_computed = p_next
        k += 1
        alpha *= 10
    
    f, ax = plt.subplots(figsize=(7, 4))
    dataFrame = pandas.DataFrame(
        px, index=cells_center[::cells[0]+1, 2], columns=cells_center[:cells[0], 0])
    ax = sns.heatmap(dataFrame, annot=True, vmin = 0, fmt='.2g', cmap='binary',
                     linewidths=0.5, linecolor='grey', cbar=False)
    ax.set_xlabel('x')
    ax.set_ylabel('z')
    plt.show()
    
    show_B(listeners, B[:, 2], Bp[:, 2])


if __name__ == "__main__":
    main()
