import seaborn as sns
from matplotlib import pyplot as plt
import pandas
import numpy as np


if __name__ == "__main__":
    while True:
        try:
            filename = input('Введите имя файла или q: ')
            if filename == "q":
                exit(0)
            else:
                px = np.loadtxt(filename, comments = "# ")
                f, ax = plt.subplots(figsize = (7, 3))
                dataFrame = pandas.DataFrame(px)
                ax = sns.heatmap(dataFrame, annot = True, fmt = '.2g', cmap = 'binary', linewidths=0.5, linecolor='grey', cbar=False)
                plt.show()
        except Exception as e:
            print(e)
